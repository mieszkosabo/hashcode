#include <bits/stdc++.h>

using namespace std;

typedef struct jol {
    uint32_t books_num, signup, ship;
    vector<pair<uint32_t, uint32_t>> books;
} library;

int main() {
    ios_base::sync_with_stdio(false);
    
    uint64_t books, libraries_num, days;

    cin >> books >> libraries_num >> days;

    vector<uint32_t> scores(books);
    vector<library> libraries(libraries_num);

    for (int i = 0; i < books; i++) {
        cin >> scores[i];
    }

    uint32_t book_jol;
    for (int i = 0; i < libraries_num; i++) {
        cin >> libraries[i].books_num;
        cin >> libraries[i].signup;
        cin >> libraries[i].ship;
        vector<pair<uint32_t, uint32_t>> jol(libraries[i].books_num);
        libraries[i].books = jol;
        for (int j = 0; j < libraries[i].books_num; j++) {
            cin >> book_jol;
            libraries[i].books[j] = {book_jol, scores[book_jol]};
        }
    }

    // cout << books << "   " << libraries_num << "   " << days << endl;

    // for (int i = 0; i < books; i++) {
    //     cout << scores[i] << "  ";
    // }
    // cout << endl << endl << endl;

    // for (int i = 0; i < libraries_num; i++) {
    //     cout << libraries[i].books_num;
    //     cout << libraries[i].signup;
    //     cout << libraries[i].ship << endl;

    //     for (int j = 0; j < libraries[i].books_num; j++) {
    //         cout << libraries[i].books[j].first << libraries[i].books[j].second << "  ";
    //     }
    //     cout << endl << endl;
    // }
}
